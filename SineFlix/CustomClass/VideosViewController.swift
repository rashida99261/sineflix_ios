//
//  VideosViewController.swift
//  SinFlix
//
//  Created by Gupta Ji on 30/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit

class VideosViewController: UIViewController {
    
    @IBOutlet weak var tblView1 : UITableView!
    var newReleaseVideosList : [video]? = [video]()
    var partySpecialVideosList : [video]? = [video]()
    var sadVideosList : [video]? = [video]()
    var someOneVideosList : [video]? = [video]()
    var trendingVideosList : [video]? = [video]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView1.register(UINib.init(nibName: "VideosTableViewCell", bundle: nil), forCellReuseIdentifier: "VideosTableViewCell")
        
        showIndicator(withTitle: "Loading...", and: "")
        ResponseData.fetchVreleaseResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.newReleaseVideosList = value.video
                ResponseData.fetchSpecialResource { (result) in
                    //PlaygroundPage.current.needsIndefiniteExecution = false
                    if let error = result.error {
                        print(error)
                    }
                    if let value = result.value {
                        self.partySpecialVideosList = value.video
                        ResponseData.fetchSadResource { (result) in
                            //PlaygroundPage.current.needsIndefiniteExecution = false
                            if let error = result.error {
                                print(error)
                            }
                            if let value = result.value {
                                self.sadVideosList = value.video
                                ResponseData.fetchSomeOneResource { (result) in
                                    //PlaygroundPage.current.needsIndefiniteExecution = false
                                    if let error = result.error {
                                        print(error)
                                    }
                                    if let value = result.value {
                                        self.someOneVideosList = value.video
                                        ResponseData.fetchTrendingAgainResource { (result) in
                                            //PlaygroundPage.current.needsIndefiniteExecution = false
                                            if let error = result.error {
                                                print(error)
                                            }
                                            if let value = result.value {
                                                self.trendingVideosList = value.video
                                                DispatchQueue.main.async {
                                                    self.tblView1.reloadData()
                                                    self.hideIndicator()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
}

extension VideosViewController: UITableViewDelegate, UITableViewDataSource, videoPlayerDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.newReleaseVideosList!.count > 0 || self.partySpecialVideosList!.count > 0 || self.sadVideosList!.count > 0 || self.someOneVideosList!.count > 0 || self.trendingVideosList!.count > 0{
            self.tblView1.backgroundView = nil
            return 5
        }
        let emptyLabel = UIImageView(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:self.view.bounds.size.height))
        emptyLabel.image = UIImage(named: "BgImage")
        self.tblView1.backgroundView = emptyLabel
        emptyLabel.contentMode = .scaleAspectFit
        self.tblView1.separatorStyle = UITableViewCell.SeparatorStyle.none
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "NEW RELEASE"
            cell.sliderVideosList = self.newReleaseVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "PARTY SPECIAL"
            cell.sliderVideosList = self.partySpecialVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "MONSOON/SAD SPECIAL"
            cell.sliderVideosList = self.sadVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "BEST OF SOMEONE"
            cell.sliderVideosList = self.someOneVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "TRENDING NOW"
            cell.sliderVideosList = self.trendingVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
    func playVideos(url : String, videoId :String, data: video){
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController")as? VideoPlayerViewController
        controller?.videoUrl = url as NSString
        controller?.videoData = data
        controller?.videoPlayFrom = "other"
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}
