//
//  AboutViewController.swift
//  SineFlix
//
//  Created by Gupta Ji on 31/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var lblAbout: UnderlinedLabel!
    @IBOutlet weak var lblContact: UnderlinedLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblAbout.text = "About Us"
        lblContact.text = "Contact / Support"
        // Do any additional setup after loading the view.
        

    }
    
    @IBAction func clickONBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

class UnderlinedLabel: UILabel {

override var text: String? {
    didSet {
        guard let text = text else { return }
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        // Add other attributes if needed
        self.attributedText = attributedText
        }
    }
}
