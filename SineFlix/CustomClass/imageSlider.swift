//
//  VideosCollectionViewCell.swift
//  SinFlix
//
//  Created by Gupta Ji on 30/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage


class imageSlider : UITableViewCell{
   
    var imagedelegate : videoPlayerDelegate!
    var sliderVideosList : [video]? = [video]()
    @IBOutlet var slideshow: ImageSlideshow!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        reloadImage()
    }
    
    func reloadImage(){
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(playSliderVideos))
        slideshow.addGestureRecognizer(gestureRecognizer)
        slideshow.slideshowInterval = 5.0
        slideshow.activityIndicator = DefaultActivityIndicator(style:.large, color: .white)
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.red
        pageControl.pageIndicatorTintColor = UIColor.white
        slideshow.pageIndicator = pageControl
        var sdWebImageSource = [SDWebImageSource]()
        if let source = sliderVideosList{
            for images in source{
                print(images.image!)
                if let thrumbnail = images.image{
                    sdWebImageSource.append(SDWebImageSource(urlString: thrumbnail.replacingOccurrences(of: " ", with: "%20"))!)
                }
            }
        }
        
        slideshow.setImageInputs(sdWebImageSource)
    }
    
    @objc func playSliderVideos(){
        imagedelegate.playVideos(url: sliderVideosList![slideshow.currentPage].video!, videoId: sliderVideosList![slideshow.currentPage].videoid!,data: sliderVideosList![slideshow.currentPage])
    }
    
}
