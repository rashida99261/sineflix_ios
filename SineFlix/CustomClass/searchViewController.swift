//
//  searchViewController.swift
//  SineFlix
//
//  Created by Apple on 11/02/21.
//

import UIKit
import SDWebImage

class searchViewController: UIViewController,  UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var txtSearch : UITextField!
    
    @IBOutlet weak var collectionView : UICollectionView!
    var seeMoreVideosList : [video] = [video]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.allreleaseVideos()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seeMoreVideosList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideosCollectionViewCell", for: indexPath as IndexPath) as? VideosCollectionViewCell
        cell?.thumbnail.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        
        let url = seeMoreVideosList[indexPath.row].image!.replacingOccurrences(of: " ", with: "%20")
        cell?.thumbnail.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "rsz_2picsart_04-30-85410"))
        cell?.title.text = seeMoreVideosList[indexPath.row].title
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 20)/2, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController")as? VideoPlayerViewController
        controller?.videoUrl = seeMoreVideosList[indexPath.row].video! as NSString
        controller?.videoData = seeMoreVideosList[indexPath.row]
        controller?.videoPlayFrom = "other"
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @IBAction func moveToBackViewController(sender :UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}


extension searchViewController {
    
    
    func allreleaseVideos(){
        ResponseData.fetchMustSeeMoreResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.seeMoreVideosList = value.video!
                if(self.seeMoreVideosList.count > 0){
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.register(UINib.init(nibName: "VideosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideosCollectionViewCell")
                    self.collectionView.reloadData()
                }
                else{
                    self.seeMoreVideosList = []
                    self.collectionView.reloadData()
                }
                
            }
        }
    }
    
    func callSearch(strSearch: String){
        ResponseData.fetchAllResourceSearch(keyword: strSearch) { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.seeMoreVideosList = value.video!
                
                print(self.seeMoreVideosList)
                
                if(self.seeMoreVideosList.count > 0){
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.register(UINib.init(nibName: "VideosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideosCollectionViewCell")
                    self.collectionView.reloadData()
                }
                else{
                    self.seeMoreVideosList = []
                    self.collectionView.reloadData()
                }
                
            }
        }
    }
}

extension searchViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = txtSearch.text
        var finalString = ""
        
        if string.isEmpty
        {
            finalString = String(string2!.dropLast())
            //textField.resignFirstResponder()
        }
        else
        {
            finalString = string2! + string1
        }
        
        if(finalString.count >= 0){
            self.seeMoreVideosList = []
            self.callSearch(strSearch: finalString)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
