//
//  HomeViewController.swift
//  SinFlix
//
//  Created by Gupta Ji on 30/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    
    @IBOutlet weak var constraints: NSLayoutConstraint!
    @IBOutlet weak var tblView : UITableView!
    var sliderVideosList : [video]? = [video]()
    var releaseVideosList : [video]? = [video]()
    var trandingVideosList : [video]? = [video]()
    var mustwatchVideosList : [video]? = [video]()
    var watchAgainVideosList : [video]? = [video]()
    var seeallVideosList : [video]? = [video]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib.init(nibName: "VideosTableViewCell", bundle: nil), forCellReuseIdentifier: "VideosTableViewCell")
        tblView.register(UINib.init(nibName: "NewRVideosTableViewCell", bundle: nil), forCellReuseIdentifier: "NewRVideosTableViewCell")
        if UIScreen.main.nativeBounds.height >= 1792.0{
            constraints.constant = constraints.constant -  44
        }else{
            constraints.constant = constraints.constant -  20
        }
        
        showIndicator(withTitle: "Loading...", and: "")
        ResponseData.fetchResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.sliderVideosList = value.video
                ResponseData.fetchReleaseResource { (result) in
                    //PlaygroundPage.current.needsIndefiniteExecution = false
                    if let error = result.error {
                        print(error)
                    }
                    if let value = result.value {
                        self.releaseVideosList = value.video
                        ResponseData.fetchTrandoingResource { (result) in
                            //PlaygroundPage.current.needsIndefiniteExecution = false
                            if let error = result.error {
                                print(error)
                            }
                            if let value = result.value {
                                self.trandingVideosList = value.video
                                ResponseData.fetchMustWatchResource { (result) in
                                    //PlaygroundPage.current.needsIndefiniteExecution = false
                                    if let error = result.error {
                                        print(error)
                                    }
                                    if let value = result.value {
                                        self.watchAgainVideosList = value.video
                                        ResponseData.fetchMustWatchAgainResource { (result) in
                                            //PlaygroundPage.current.needsIndefiniteExecution = false
                                            if let error = result.error {
                                                print(error)
                                            }
                                            if let value = result.value {
                                                self.mustwatchVideosList = value.video
                                                DispatchQueue.main.async {
                                                    self.tblView.reloadData()
                                                    self.hideIndicator()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource, videoPlayerDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sliderVideosList!.count > 0 || self.releaseVideosList!.count > 0 || self.trandingVideosList!.count > 0 || self.mustwatchVideosList!.count > 0 || self.watchAgainVideosList!.count > 0{
            self.tblView.backgroundView = nil
            return 5
        }
        let emptyLabel = UIImageView(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:self.view.bounds.size.height))
        emptyLabel.image = UIImage(named: "BgImage")
        emptyLabel.contentMode = .scaleAspectFit
        self.tblView.backgroundView = emptyLabel
        self.tblView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageSlider")as! imageSlider
            //tableView.dequeueReusableCell(withIdentifier: "imageSlider", for: indexPath as IndexPath)
            cell.sliderVideosList = self.sliderVideosList
            cell.imagedelegate = self
            cell.reloadImage()
            return cell
            
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewRVideosTableViewCell", for: indexPath as IndexPath) as! NewRVideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.rightTitle.addTarget(self, action: #selector(allreleaseVideos), for: .touchUpInside)
            cell.leftTitle.text = "NEW RELEASE"
            cell.sliderVideosList = self.releaseVideosList!
            cell.backgroundColor = UIColor.red
            
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.rightTitle.addTarget(self, action: #selector(alltrandingVideos), for: .touchUpInside)
            cell.leftTitle.text = "TRENDING NOW"
            cell.sliderVideosList = self.trandingVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.rightTitle.addTarget(self, action: #selector(allmustwatchVideos), for: .touchUpInside)
            cell.leftTitle.text = "MUST WATCH"
            cell.sliderVideosList = self.mustwatchVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.rightTitle.addTarget(self, action: #selector(allwatchagainVideos), for: .touchUpInside)
            cell.leftTitle.text = "WATCH AGAIN"
            cell.sliderVideosList = self.watchAgainVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return 250
        }
        return 260
    }
    
    func playVideos(url : String, videoId: String, data: video){
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController
        controller?.videoUrl = url as NSString
        controller?.videoData = data
        controller?.videoPlayFrom = "other"
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @objc func allreleaseVideos(){
        ResponseData.fetchMustSeeMoreResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.seeallVideosList = value.video
                self.moveToSeeAllVideos(allVideos: self.seeallVideosList!)
            }
        }
    }
    
    @objc func allmustwatchVideos(){
        ResponseData.fetchMustSeeMoreResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.seeallVideosList = value.video
                self.moveToSeeAllVideos(allVideos: self.seeallVideosList!)
            }
        }
    }
    
    @objc func allwatchagainVideos(){
        ResponseData.fetchWatchAgainSeeMoretResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.seeallVideosList = value.video
                self.moveToSeeAllVideos(allVideos: self.seeallVideosList!)
            }
        }
    }
    
    @objc func alltrandingVideos(){
        ResponseData.fetchTrendingSeeMorResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.seeallVideosList = value.video
                self.moveToSeeAllVideos(allVideos: self.seeallVideosList!)
            }
        }
    }
    
    func moveToSeeAllVideos(allVideos :[video]){
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoDetailsViewController")as? VideoDetailsViewController
        controller?.seeMoreVideosList = allVideos
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    
}

