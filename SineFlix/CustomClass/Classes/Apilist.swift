//
//  Apilist.swift
//  SineFlix
//
//  Created by Gupta Ji on 08/06/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import Foundation

class API: NSObject {
    static let  baseUrl = "http://webdemoservice.online/sineflix_latest/"
    
    enum URLPath {
        case release
        case slider
        case trending
        case trendingseemore
        case must
        case mustseemore
        case watchagain
        case vrelease
        case views
        case wbest
        case whot
        case wreleased
        case wcrime
        case mspecial
        case msomeone
        case mthriller
        case maction
        case mromance
        case logo
        case alllanguage
        case subcategory
        case category
        case watchagainseemore
        case vspecial
        case vsad
        case vsomeone
        case vtrending
        case language
        case videodetails
        case upload_single_image
        case usercreate
        case profile
        case profileupdate
        case cpassword
        case login
        case playlistremove
        case playlist
        case playlistget
        case save
        case saveget
        case chanellist
        case subscibeclick
        case subscibe
        case chanel
        case videolike
        case commentsget
        case comments
        case videodislikeget
        case videolikeget
        case commentsdelete
        case searchvideo
        case mailverify
        case otpmatch
        case releaseseemore
        
        
        
        var path: String {
            switch self {
            case .slider:
                return "slider"
            case .release:
                return "release"
            case .trending:
                return "trending"
            case .trendingseemore:
                return "trendingseemore"
            case .must:
                return "must"
            case .mustseemore:
                return "mustseemore"
            case .watchagain:
                return "watchagain"
            case .vrelease:
                return "vrelease"
            case .views:
                return "views"
            case .wbest:
                return "wbest"
            case .whot:
                return "whot"
            case .wreleased:
                return "wreleased"
            case .wcrime:
                return "wcrime"
            case .mspecial:
                return "mspecial"
            case .msomeone:
                return "msomeone"
            case .mthriller:
                return "mthriller"
            case .maction:
                return "maction"
            case .mromance:
                return "mromance"
            case .logo:
                return "logo"
            case .alllanguage:
                return "alllanguage"
            case .subcategory:
                return "subcategory"
            case .category:
                return "category"
            case .watchagainseemore:
                return "watchagainseemore"
            case .vspecial:
                return "vspecial"
            case .vsad:
                return "vsad"
            case .vsomeone:
                return "vsomeone"
            case .vtrending:
                return "vtrending"
            case .language:
                return "language"
            case .videodetails:
                return "videodetails"
            case .upload_single_image:
                return "upload_single_image"
            case .usercreate:
                return "usercreate"
            case .profile:
                return "profile"
            case .profileupdate:
                return "profileupdate"
            case .cpassword:
                return "cpassword"
            case .login:
                return "login"
            case .playlistremove:
                return "playlistremove"
            case .playlist:
                return "playlist"
            case .playlistget:
                return "playlistget"
            case .save:
                return "save"
            case .saveget:
                return "saveget"
            case .chanellist:
                return "chanellist"
            case .subscibeclick:
                return "subscibeclick"
            case .subscibe:
                return "subscibe"
            case .chanel:
                return "chanel"
            case .videolike:
                return "videolike"
            case .commentsget:
                return "mustseemore"
            case .comments:
                return "comments"
            case .videodislikeget:
                return "videodislikeget"
            case .videolikeget:
                return "videolikeget"
            case .commentsdelete:
                return "commentsdelete"
            case .searchvideo:
                return "searchvideo"
            case .mailverify:
                return "mailverify"
            case .otpmatch:
                return "otpmatch"
            case .releaseseemore:
                return "releaseseemore"
            }
        }
    }
    
    static func createUrl(endpoint: URLPath) -> String {
        return "\(API.baseUrl)\(endpoint.path)"
    }
}


