//
//  MoviesViewController.swift
//  SinFlix
//
//  Created by Gupta Ji on 30/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController {
    
    @IBOutlet weak var tblView3 : UITableView!
    var romanceVideosList : [video]? = [video]()
    var actionVideosList : [video]? = [video]()
    var thrillerVideosList : [video]? = [video]()
    var someoneVideosList : [video]? = [video]()
    var specialVideosList : [video]? = [video]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView3.register(UINib.init(nibName: "VideosTableViewCell", bundle: nil), forCellReuseIdentifier: "VideosTableViewCell")
        showIndicator(withTitle: "Loading...", and: "")
        ResponseData.fetchBestInRomanceResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.romanceVideosList = value.video
                ResponseData.fetchBestInActionResource { (result) in
                    //PlaygroundPage.current.needsIndefiniteExecution = false
                    if let error = result.error {
                        print(error)
                    }
                    if let value = result.value {
                        self.actionVideosList = value.video
                        ResponseData.fetchBestInThrillerResource { (result) in
                            //PlaygroundPage.current.needsIndefiniteExecution = false
                            if let error = result.error {
                                print(error)
                            }
                            if let value = result.value {
                                self.thrillerVideosList = value.video
                                ResponseData.fetchBestOfSomeOneResource { (result) in
                                    //PlaygroundPage.current.needsIndefiniteExecution = false
                                    if let error = result.error {
                                        print(error)
                                    }
                                    if let value = result.value {
                                        self.someoneVideosList = value.video
                                        ResponseData.fetchSomeOneSpecialAgainResource { (result) in
                                            //PlaygroundPage.current.needsIndefiniteExecution = false
                                            if let error = result.error {
                                                print(error)
                                            }
                                            if let value = result.value {
                                                self.specialVideosList = value.video
                                                DispatchQueue.main.async {
                                                    self.tblView3.reloadData()
                                                    self.hideIndicator()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension MoviesViewController: UITableViewDelegate, UITableViewDataSource, videoPlayerDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.romanceVideosList!.count > 0 || self.actionVideosList!.count > 0 || self.thrillerVideosList!.count > 0 || self.someoneVideosList!.count > 0 || self.specialVideosList!.count > 0{
         self.tblView3.backgroundView = nil
         return 5
        }
        let emptyLabel = UIImageView(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:self.view.bounds.size.height))
        emptyLabel.image = UIImage(named: "BgImage")
        emptyLabel.contentMode = .scaleAspectFit
        self.tblView3.backgroundView = emptyLabel
        self.tblView3.separatorStyle = UITableViewCell.SeparatorStyle.none
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "Best In Romance"
            cell.sliderVideosList = self.romanceVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "Best In Action"
            
            cell.sliderVideosList = self.actionVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "Best In Thriller"
            if(self.thrillerVideosList != nil){
                cell.sliderVideosList = self.thrillerVideosList!
                cell.backgroundColor = UIColor.red
                cell.collectionView.reloadData()
            }
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "Best of SomeOne"
            if(self.someoneVideosList != nil){
                cell.sliderVideosList = self.someoneVideosList!
                cell.backgroundColor = UIColor.red
                cell.collectionView.reloadData()
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
            cell.delegate = self
            cell.rightTitle.setTitle("See More", for: .normal)
            cell.leftTitle.text = "SomeOne Special"
            cell.sliderVideosList = self.specialVideosList!
            cell.backgroundColor = UIColor.red
            cell.collectionView.reloadData()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 1{
//            return 250
//        }
//        return 300
   // }
    
    func playVideos(url : String, videoId: String, data: video){
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController")as? VideoPlayerViewController
        controller?.videoUrl = url as NSString
        controller?.videoData = data
        controller?.videoPlayFrom = "other"
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}
