//
//  WebShowViewController.swift
//  SinFlix
//
//  Created by Gupta Ji on 30/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

class WebShowViewController: UIViewController {
    
    @IBOutlet weak var tblView2 : UITableView!
    var crimeVideosList : [video]? = [video]()
    var newReleaseVideosList : [video]? = [video]()
    var hotpickVideosList : [video]? = [video]()
    var BesteofthebestVideosList : [video]? = [video]()
    //var trendingVideosList : [video]? = [video]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView2.backgroundColor = .black
        tblView2.register(UINib.init(nibName: "VideosTableViewCell", bundle: nil), forCellReuseIdentifier: "VideosTableViewCell")
        showIndicator(withTitle: "Loading..", and: "")
        ResponseData.fetchCrimeResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.crimeVideosList = value.video
                ResponseData.fetchWReleaseResource { (result) in
                    //PlaygroundPage.current.needsIndefiniteExecution = false
                    if let error = result.error {
                        print(error)
                    }
                    if let value = result.value {
                        self.newReleaseVideosList = value.video
                        ResponseData.fetchHotPickResource { (result) in
                            //PlaygroundPage.current.needsIndefiniteExecution = false
                            if let error = result.error {
                                print(error)
                            }
                            if let value = result.value {
                                self.hotpickVideosList = value.video
                                ResponseData.fetchBestofthebestResource { (result) in
                                    //PlaygroundPage.current.needsIndefiniteExecution = false
                                    if let error = result.error {
                                        print(error)
                                    }
                                    if let value = result.value {
                                        self.BesteofthebestVideosList = value.video
                                        DispatchQueue.main.async {
                                            self.tblView2.reloadData()
                                            self.hideIndicator()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
extension WebShowViewController: UITableViewDelegate, UITableViewDataSource, videoPlayerDelegate{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if self.crimeVideosList!.count > 0 || self.newReleaseVideosList!.count > 0 || self.hotpickVideosList!.count > 0 || self.BesteofthebestVideosList!.count > 0{
                
                tblView2.backgroundView = nil
//                tblView2.isHidden = false
                return 4
            }
            let emptyLabel = UIImageView(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:self.view.bounds.size.height))
            emptyLabel.image = UIImage(named: "BgImage")
            emptyLabel.contentMode = .scaleAspectFit
            self.tblView2.backgroundView = emptyLabel
            self.tblView2.separatorStyle = UITableViewCell.SeparatorStyle.none
            //tblView2.isHidden = true
            return 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
                cell.delegate = self
                cell.rightTitle.setTitle("See More", for: .normal)
                cell.leftTitle.text = "CRIME"
                cell.sliderVideosList = self.crimeVideosList!
                cell.backgroundColor = UIColor.red
                cell.collectionView.reloadData()
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
                cell.delegate = self
                cell.rightTitle.setTitle("See More", for: .normal)
                cell.leftTitle.text = "NEW RELEASED"
                cell.sliderVideosList = self.newReleaseVideosList!
                cell.backgroundColor = UIColor.red
                cell.collectionView.reloadData()
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
                cell.delegate = self
                cell.rightTitle.setTitle("See More", for: .normal)
                cell.leftTitle.text = "HOT PICK"
                cell.sliderVideosList = self.hotpickVideosList!
                cell.backgroundColor = UIColor.red
                cell.collectionView.reloadData()
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "VideosTableViewCell", for: indexPath as IndexPath) as! VideosTableViewCell
                cell.delegate = self
                cell.rightTitle.setTitle("See More", for: .normal)
                cell.leftTitle.text = "BESTE OF THE BEST"
                cell.sliderVideosList = self.BesteofthebestVideosList!
                cell.backgroundColor = UIColor.red
                cell.collectionView.reloadData()
                return cell
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 260
        }
        
    func playVideos(url : String, videoId: String, data: video){
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController")as? VideoPlayerViewController
        controller?.videoUrl = url as NSString
        controller?.videoData = data
        controller?.videoPlayFrom = "other"
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    func videoDetails(){
        ResponseData.fetchBestofthebestResource { (result) in
            //PlaygroundPage.current.needsIndefiniteExecution = false
            if let error = result.error {
                print(error)
            }
            if let value = result.value {
                self.BesteofthebestVideosList = value.video
                DispatchQueue.main.async {
                    self.tblView2.reloadData()
                    self.hideIndicator()
                }
            }
        }
    }
}
