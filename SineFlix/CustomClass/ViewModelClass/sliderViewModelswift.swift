////
////  slider.ViewModelswift.swift
////  SineFlix
////
////  Created by Gupta Ji on 08/06/2020.
////  Copyright © 2020 Vivek Gupta. All rights reserved.
////
//
import Foundation

struct ResponseData: Codable {
    var status: String?
    var msg: String?
    var video: [video]?
}

extension ResponseData: CustomDebugStringConvertible {
    var debugDescription: String {
        return "\n{status:\(status ?? ""), msg:\(msg  ?? ""), videos: \(video ?? []))}"
    }
}

struct video : Codable {
    var video: String?
    var videoid: String?
    var category: String?
    var lang : String?
    var subcategory: String?
    var title: String?
    var description : String?
    var cdate : String?
    var image : String?
    
}

extension video: CustomDebugStringConvertible {
    var debugDescription: String {
        return "\n{video:\(video ?? ""), videoid:\(videoid ?? ""), category:\(category ?? ""), lang:\(lang ?? ""), subcategory: \(subcategory ?? ""), title: \(title ?? ""), description: \(description ?? ""), cdate: \(cdate ?? ""), image: \(image ?? "")}"
    }
}
