//
//  Model.swift
//  SineFlix
//
//  Created by Gupta Ji on 09/06/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import Foundation

protocol APIModel {
    var status: String { get set }
}

typealias APIModelCodable = APIModel & Codable

extension APIModel where Self: Codable {
    static func from(json: String, using encoding: String.Encoding = .utf8) -> Self? {
        guard let data = json.data(using: encoding) else { return nil }
        return from(data: data)
    }
    
    static func from(data: Data) -> Self? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return try? decoder.decode(Self.self, from: data)
    }
    
    var jsonData: Data? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        return try? encoder.encode(self)
    }
    
    var jsonString: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}



