//
//  SliderService.swift
//  SineFlix
//
//  Created by Gupta Ji on 09/06/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD

class WebResourceManager {
    static let shared = WebResourceManager()
    
    func fetchRequest<A>(resource: WebResource<A>, completion: @escaping (Result<A>)->Void) -> RequestToken {
        let url = resource.urlPath.url!
        let header = resource.header
        let parameter = resource.method.parameter
        
        print(url)
        
        let dataRequest = Alamofire.request(url, method: resource.method.requestMethod, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseData { (response) in
            
            if let code = response.response?.statusCode {
                if 200...299 ~= code {
                    print("All well")
                }
                if 300...399 ~= code {
                    print("Huhh")
                }
                if 400...499 ~= code {
                    let nserror = NSError(domain: "com.neilsultimatelab.errorbase", code: code, userInfo: nil)
                    completion(.error(.serverError(nserror)))
                    return
                }
            }
            
            if let error = response.result.error {
                print(error)
                completion(.error(.serverError(error)))
                return
            }
            if response.result.isSuccess {
                if let data = response.result.value {
                    completion(resource.decode(data))
                    return
                }
            }else {
                completion(.error(.missingData))
                return
            }
        }
        
        let task = dataRequest.task
        
        return RequestToken(task: task)
    }
}

class RequestToken {
    private weak var task: URLSessionTask?
    
    init(task: URLSessionTask?) {
        self.task = task
    }
    
    func cancel() {
        self.task?.cancel()
    }
}


struct WebResource<A> {
    var urlPath: URLPath
    var method: HTTPMethod = .get
    var header: [String : String]? = [:]
    var decode: (Data) -> Result<A>
    func request(completion: @escaping (Result<A>)->Void) -> RequestToken {
        return WebResourceManager.shared.fetchRequest(resource: self, completion: completion)
    }
}


enum Result<A> {
    case value(A)
    case error(AppError)
    
    var error: AppError? {
        switch self {
        case .error(let b):
            return b
        default:
            return nil
        }
    }
    
    var value: A? {
        switch self {
        case .value(let a):
            return a
        default:
            return nil
        }
    }
}


enum AppError: Error {
    case missingData
    case serverError(Error)
    case sessionExpired
    case notReachable
}


typealias JSONType = [String : Any]

enum HTTPMethod {
    case get
    case post(JSONType?)
    case put(JSONType?)
    
    var requestMethod: Alamofire.HTTPMethod {
        switch self {
        case .get:
            return Alamofire.HTTPMethod.get
        case .post(_):
            return Alamofire.HTTPMethod.post
        case .put(_):
            return Alamofire.HTTPMethod.put
        }
    }
    
    var parameter: JSONType? {
        switch self {
        case let .post(parameter):
            return parameter
        case let .put(parameter):
            return parameter
        default:
            return nil
        }
    }
}



struct AppConfig {
    static let scheme: URLScheme = .http
    //static let host: URLHost = .live
}


enum URLPath {
    case release
    case slider
    case trending
    case trendingseemore
    case must
    case mustseemore
    case watchagain
    case vrelease
    case views
    case wbest
    case whot
    case wreleased
    case wcrime
    case mspecial
    case msomeone
    case mthriller
    case maction
    case mromance
    case logo
    case alllanguage
    case subcategory
    case category
    case watchagainseemore
    case vspecial
    case vsad
    case vsomeone
    case vtrending
    case language
    case videodetails
    case upload_single_image
    case usercreate
    case profile
    case profileupdate
    case cpassword
    case login
    case playlistremove
    case playlist
    case playlistget
    case save
    case saveget
    case chanellist
    case subscibeclick
    case subscibe
    case chanel
    case videolike
    case commentsget
    case comments
    case videodislikeget
    case videolikeget
    case commentsdelete
    case searchvideo
    case mailverify
    case otpmatch
    case releaseseemore
    case userSubscriptionStatus
    case user_subscription
    case get_subscription_plan
    case contact_support
    
    var path: String {
        switch self {
        case .slider:
            return "slider"
        case .release:
            return "release"
        case .trending:
            return "trending"
        case .trendingseemore:
            return "trendingseemore"
        case .must:
            return "must"
        case .mustseemore:
            return "mustseemore"
        case .watchagain:
            return "watchagain"
        case .vrelease:
            return "vrelease"
        case .views:
            return "views"
        case .wbest:
            return "wbest"
        case .whot:
            return "whot"
        case .wreleased:
            return "wreleased"
        case .wcrime:
            return "wcrime"
        case .mspecial:
            return "mspecial"
        case .msomeone:
            return "msomeone"
        case .mthriller:
            return "mthriller"
        case .maction:
            return "maction"
        case .mromance:
            return "mromance"
        case .logo:
            return "logo"
        case .alllanguage:
            return "alllanguage"
        case .subcategory:
            return "subcategory"
        case .category:
            return "category"
        case .watchagainseemore:
            return "watchagainseemore"
        case .vspecial:
            return "vspecial"
        case .vsad:
            return "vsad"
        case .vsomeone:
            return "vsomeone"
        case .vtrending:
            return "vtrending"
        case .language:
            return "language"
        case .videodetails:
            return "videodetails"
        case .upload_single_image:
            return "upload_single_image"
        case .usercreate:
            return "usercreate"
        case .profile:
            return "profile"
        case .profileupdate:
            return "profileupdate"
        case .cpassword:
            return "cpassword"
        case .login:
            return "login"
        case .playlistremove:
            return "playlistremove"
        case .playlist:
            return "playlist"
        case .playlistget:
            return "playlistget"
        case .save:
            return "save"
        case .saveget:
            return "saveget"
        case .chanellist:
            return "chanellist"
        case .subscibeclick:
            return "subscibeclick"
        case .subscibe:
            return "subscibe"
        case .chanel:
            return "chanel"
        case .videolike:
            return "videolike"
        case .commentsget:
            return "mustseemore"
        case .comments:
            return "comments"
        case .videodislikeget:
            return "videodislikeget"
        case .videolikeget:
            return "videolikeget"
        case .commentsdelete:
            return "commentsdelete"
        case .searchvideo:
            return "searchvideo"
        case .mailverify:
            return "mailverify"
        case .otpmatch:
            return "otpmatch"
        case .releaseseemore:
            return "releaseseemore"
        case .userSubscriptionStatus:
            return "userSubscriptionStatus"
        case .user_subscription:
            return "user_subscription"
        case .get_subscription_plan:
            return "get_subscription_plan"
        case .contact_support:
            return "contact_support"
        }
        
    }
    
    var url: URL? {
        return URL(string: "http://webdemoservice.online/sineflix_latest/ApiController/" + path)
    }
}

//"http://webdemoservice.online/sineflix_update/"

enum URLHost: String {
    case live = "sineflix.in/ApiController/"
    
    var host: String {
        return self.rawValue
    }
    
    var fixedPath: String {
        return ""
    }
}


enum URLScheme: String {
    case http
    case https
    
}

extension ResponseData {
    static func fetchResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .slider, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchReleaseResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .release, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchTrandoingResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .trending, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchMustWatchResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .must, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchMustWatchAgainResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .watchagain, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    
    static func fetchBestInRomanceResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .mromance, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchBestInActionResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .maction, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchBestInThrillerResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .mthriller, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchBestOfSomeOneResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .msomeone, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchSomeOneSpecialAgainResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .mspecial, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    
    
    static func fetchVreleaseResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .vrelease, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchSpecialResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .vspecial, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchSadResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .vsad, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchSomeOneResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .vsomeone, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchTrendingAgainResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .vtrending, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    
    static func fetchCrimeResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .wcrime, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchWReleaseResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .wreleased, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchHotPickResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .whot, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchBestofthebestResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .wbest, method: .get, header: nil, decode: (ResponseData.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
    
    static func fetchReleaseSeeMoreResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
           let webResource = WebResource<ResponseData>(urlPath: .releaseseemore, method: .get, header: nil, decode: (ResponseData.decode))
           return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
       }
    
    static func fetchWatchAgainSeeMoretResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
           let webResource = WebResource<ResponseData>(urlPath: .watchagainseemore, method: .get, header: nil, decode: (ResponseData.decode))
           return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
       }
    
    static func fetchMustSeeMoreResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .mustseemore, method: .get, header: nil, decode: (ResponseData.decode))
           return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
       }
    
    static func fetchTrendingSeeMorResource(completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
           let webResource = WebResource<ResponseData>(urlPath: .trendingseemore, method: .get, header: nil, decode: (ResponseData.decode))
           return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
       }
    
    static func fetchAllResourceSearch(keyword: String, completion: @escaping (Result<ResponseData>)->Void) -> RequestToken {
        let webResource = WebResource<ResponseData>(urlPath: .searchvideo, method: .post(["Search":keyword]), header: nil, decode: (ResponseData.decode))
           return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
       }
}

extension Decodable {
    static func decode<T: Codable>(_ data: Data) -> Result<T> {
        if let decoded = try? JSONDecoder().decode(T.self, from: data) {
            return .value(decoded)
        }
        return .error(.missingData)
    }
}

extension UIViewController {
   func showIndicator(withTitle title: String, and Description:String) {
      let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
      Indicator.label.text = title
      Indicator.isUserInteractionEnabled = false
      Indicator.detailsLabel.text = Description
      Indicator.show(animated: true)
   }
   func hideIndicator() {
      MBProgressHUD.hide(for: self.view, animated: true)
   }
}


struct Endpoints {
    struct Environment {
        static let baseUrl = "http://webdemoservice.online/sineflix_latest/ApiController/"
        
        //"https://sineflix.in/ApiController"
        
        static let rayzorPayKey = "rzp_test_q0YRR7fFoRg9eK"
        
        
        static var arrMenuLogout = [ ["title":"Legal","img":"accept"],
                                    ["title":"Subscribe","img":"sbuscribe"],
                                    ["title":"About","img":"about"],
                                    ["title":"Rate Us","img":"star"],
                                    ["title":"Share App","img":"share"],
                                    ["title":"Contact Us","img":"contact"],
                                    ["title":"Login","img":"login"]]
        
        static var arrMenuLogin = [ ["title":"Profile","img":"downloads"],
                                    ["title":"Legal","img":"accept"],
                                    ["title":"Subscribe","img":"sbuscribe"],
                                    ["title":"About","img":"about"],
                                    ["title":"My Watch List","img":"about"],
                                    ["title":"Rate Us","img":"star"],
                                    ["title":"Share App","img":"share"],
                                    ["title":"Contact Us","img":"contact"],
                                    ["title":"Logout","img":"login"]]
    }
}
