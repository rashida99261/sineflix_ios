//
//  splashViewController.swift
//  SineFlix
//
//  Created by Apple on 10/02/21.
//

import UIKit

class splashViewController: UIViewController {
    
    @IBOutlet weak var imgGif: UIImageView!
    
    var gameTimer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "splashSineFlix", withExtension: "gif")!)
        let advTimeGif = UIImage.gifImageWithData(imageData!)
        self.imgGif.image = advTimeGif
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
            // your function here
            self.navigateToViewController()
        }
        

    }
    
    
    func navigateToViewController(){
        let controller = self.storyboard?.instantiateViewController(identifier: "ViewController")as? ViewController
        self.navigationController?.pushViewController(controller!, animated: false)
    }
    

    

}
