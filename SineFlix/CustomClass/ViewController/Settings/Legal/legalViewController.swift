//
//  legalViewController.swift
//  SineFlix
//
//  Created by Apple on 05/02/21.
//

import UIKit

class legalViewController: UIViewController {

    
    var pageMenu : CAPSPageMenu?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var controllerArray : [UIViewController] = []
        
        let faq = self.storyboard?.instantiateViewController(withIdentifier: "faqViewController")
        faq!.title = "FAQ"
        controllerArray.append(faq!)
        
        let moviesViewController = self.storyboard?.instantiateViewController(withIdentifier: "privacyPolicyViewController")
        moviesViewController!.title = "PRIVACY POLICY"
        controllerArray.append(moviesViewController!)
        
        let webShowViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsViewController")
        webShowViewController!.title = "TERM&CONDITION"
        controllerArray.append(webShowViewController!)
        
        
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .bottomMenuHairlineColor(UIColor.white),
            .menuItemSeparatorColor(UIColor.clear),
            .scrollMenuBackgroundColor(UIColor.red),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.white),
            //.menuHeight(50),
            //.centerMenuItems(true),
            .menuItemFont(UIFont.boldSystemFont(ofSize: 14)),
            .menuItemSeparatorWidth(4.3),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        //pageMenu!.delegate = self
        pageMenu!.didMove(toParent: self)
        navigationController?.navigationBar.barTintColor = UIColor.red
        // Customize menu (Optional)
//        let parameters: [CAPSPageMenuOption] = [
//            .scrollMenuBackgroundColor(UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)),
//            .viewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
//            .selectionIndicatorColor(UIColor.orange),
//            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
//            .menuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
//            .menuHeight(40.0),
//            .menuItemWidth(90.0),
//            .centerMenuItems(true)
//        ]
//
//        // Initialize scroll menu
//        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
//
//        self.addChild(pageMenu!)
//        self.view.addSubview(pageMenu!.view)
//
//        pageMenu!.didMove(toParent: self)
    }
    
//    @objc func didTapGoToLeft() {
//        let currentIndex = pageMenu!.currentPageIndex
//
//        if currentIndex > 0 {
//            pageMenu!.moveToPage(currentIndex - 1)
//        }
//    }
//
//    @objc func didTapGoToRight() {
//        let currentIndex = pageMenu!.currentPageIndex
//
////        if currentIndex < pageMenu!.controllerArray.count {
////            pageMenu!.moveToPage(currentIndex + 1)
////        }
//    }
    
    // MARK: - Container View Controller

    //COULD NOT RESOLVE
//    override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
//        return true
//    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}


