//
//  subscribeViewController.swift
//  SineFlix
//
//  Created by Apple on 07/02/21.
//

import UIKit
import Razorpay
import SafariServices

class subscribeViewController: UIViewController {
    
    @IBOutlet weak var viewSubscribed: UIView!
    @IBOutlet weak var viewUnSubscribed: UIView!
    
    var razorpayObj : RazorpayCheckout? = nil
    var merchantDetails : MerchantsDetails = MerchantsDetails.getDefaultData()
    
    var subscriptionId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.viewSubscribed.isHidden = true
        self.viewUnSubscribed.isHidden = true
        self.checkStats()
    }
    
    @IBAction func clikOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension subscribeViewController {
    
    func checkStats(){
        showIndicator(withTitle: "", and: "")
        do {
            let decoded  = userDef.object(forKey: "userInfo") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let uId = responseDict["id"] as! String
                let params = [ "user_id": uId] as [String : Any]
                let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.userSubscriptionStatus)"
                let url = URL(string: strUrl)
                ApiService.callPost(url: url!, params: params, finish: parseApiResponse)
            }
        }
        catch{
        }
    }
    
    func parseApiResponse(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    //let msg = json["msg"] as! String
                    if(status == "success"){
                        
                        if let isSubscriptionActive = json["isSubscriptionActive"] as? String{
                            if(isSubscriptionActive == "true"){
                                self.viewSubscribed.isHidden = false
                                self.viewUnSubscribed.isHidden = true
                            }
                            else{
                                self.viewSubscribed.isHidden = true
                                self.viewUnSubscribed.isHidden = false
                                self.getSubscriptionPlan()
                            }
                        }
                        
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func clickOnViewPay(_ sender: UIButton)
    {
        razorpayObj = RazorpayCheckout.initWithKey(Endpoints.Environment.rayzorPayKey, andDelegate: self)
        self.openRazorpayCheckout()
    }
}

extension subscribeViewController {
    
    private func openRazorpayCheckout() {
        
        let options: [AnyHashable:Any] = [
            "prefill": [
                "contact": "",
                "email": ""
            ],
            "image": merchantDetails.logo,
            "amount" : 9900,
            "name": merchantDetails.name,
            "theme": [
                "color": merchantDetails.color.toHexString()
            ]
        ]
        if let rzp = self.razorpayObj {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }
    }
}



// RazorpayPaymentCompletionProtocol - This will execute two methods 1.Error and 2. Success case. On payment failure you will get a code and description. In payment success you will get the payment id.
extension subscribeViewController : RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        print("error: ", code, str)
        self.presentAlert(withTitle: "Alert", message: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        print("success: ", payment_id)
        self.passPaymentIdToapi(pay_id: payment_id)
    }
}


extension subscribeViewController {
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension subscribeViewController : SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

protocol CustomizeDataDelegate {
    func dataChanged(with merchantDetails : MerchantsDetails)
}

struct MerchantsDetails {
    let name : String
    let logo : String
    let color : UIColor
}

extension MerchantsDetails {
    
    static func getDefaultData() -> MerchantsDetails {
        let details = MerchantsDetails(name: "SineFlix",
                                       logo: "https://pbs.twimg.com/profile_images/1271385506505347074/QIc_CCEg_400x400.jpg",
                                       color: .systemRed)
        return details
    }
}

extension subscribeViewController {
    
    func getSubscriptionPlan(){
        showIndicator(withTitle: "", and: "")
        do {
            let decoded  = userDef.object(forKey: "userInfo") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let uId = responseDict["id"] as! String
                let params = [ "user_id": uId] as [String : Any]
                let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.get_subscription_plan)"
                let url = URL(string: strUrl)
                ApiService.callPost(url: url!, params: params, finish: parseApiResponse_Paymnt)
            }
        }
        catch{
        }
    }
    
    func passPaymentIdToapi(pay_id: String){
        showIndicator(withTitle: "", and: "")
        do {
            let decoded  = userDef.object(forKey: "userInfo") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let uId = responseDict["id"] as! String
                let params = [ "user_id": uId, "plan_id": "", "is_valid":1, "razorpayPaymentID": pay_id] as [String : Any]
                let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.user_subscription)"
                let url = URL(string: strUrl)
                ApiService.callPost(url: url!, params: params, finish: parseApiResponse_Paymnt)
            }
        }
        catch{
        }
    }
    
    func parseApiResponse_Paymnt(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    //let msg = json["msg"] as! String
                    if(status == "success"){
//                        self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
//                        self.viewDidLoad()
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func parseApiResponse_susbcriptionTyp(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    //let msg = json["msg"] as! String
                    if(status == "success"){
                        
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}




