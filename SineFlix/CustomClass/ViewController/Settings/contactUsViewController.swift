//
//  contactUsViewController.swift
//  SineFlix
//
//  Created by Apple on 04/02/21.
//

import UIKit

class contactUsViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtSelect: UITextField!
    @IBOutlet weak var txtMsg: UITextView!
    
    
    var pickerSelect : UIPickerView!
    var arrSelectTyp = ["Billing Issue","Streaming Issue","Content Copyright", "Become a Partner","Application Issue","Login Issue","Other"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtMsg.text = "Message"
        txtMsg.textColor = UIColor.lightGray
        
        
        // Do any additional setup after loading the view.
    }
    
    
}

extension contactUsViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
    
}

extension contactUsViewController {
    
    @IBAction func clickONBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickONSubmit(_ sender: UIButton)
    {
        if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill email field.")
        }
        else if(!(ApiService.isValidEmail(testStr: txtEmail.text!))){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill valid email field.")
        }
        else if(txtMobile.text == "" || txtMobile.text?.count == 0 || txtMobile.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill mobile number field.")
        }
        else if(txtSelect.text == "" || txtSelect.text?.count == 0 || txtSelect.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please select field.")
        }
        else if(txtMsg.text == "" || txtMsg.text?.count == 0 || txtMsg.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill message field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            showIndicator(withTitle: "", and: "")
            self.calContactusApi()
        }
        else{
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "PLease check your internet connection.")
        }
    }
    
}


extension contactUsViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerSelect{
            return self.arrSelectTyp.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerSelect{
            let strTitle = arrSelectTyp[row]
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerSelect{
            let strTitle = arrSelectTyp[row]
            self.txtSelect.text = strTitle
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtSelect){
            self.pickUp(txtSelect)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtSelect){
            
            self.pickerSelect = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSelect.delegate = self
            self.pickerSelect.dataSource = self
            self.pickerSelect.backgroundColor = UIColor.white
            textField.inputView = self.pickerSelect
        }
        
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtSelect.resignFirstResponder()
        
    }
    
    @objc func cancelClick() {
        txtSelect.resignFirstResponder()
    }
    
    
}


extension contactUsViewController {
    
    func calContactusApi(){
        
        let params = ["email": self.txtEmail.text!,
                      "mobile": self.txtMobile.text!,
                      "query_type": self.txtSelect.text!,
                      "message": self.txtMsg.text! ] as [String : Any]
        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.contact_support)"
        let url = URL(string: strUrl)
        ApiService.callPost(url: url!, params: params, finish: callApitoGetWatchList)
    }
    
    func callApitoGetWatchList(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Submit successfully!")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
