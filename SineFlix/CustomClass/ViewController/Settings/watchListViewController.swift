//
//  watchListViewController.swift
//  SineFlix
//
//  Created by Apple on 06/02/21.
//

import UIKit
import SDWebImage

class watchListViewController: UIViewController {

    @IBOutlet weak var collWatchList: UICollectionView!
    
    var watchVideosList : [NSDictionary] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.watchVideosList = []
        showIndicator(withTitle: "", and: "")
        self.callWachPlayListApi()
    }
    
    @IBAction func clickONBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}


extension watchListViewController {
    
    func callWachPlayListApi(){
        do {
            let decoded  = userDef.object(forKey: "userInfo") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let uId = responseDict["id"] as! String
                let params = ["id": uId] as [String : Any]
                let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.playlistget)"
                let url = URL(string: strUrl)
                ApiService.callPost(url: url!, params: params, finish: callApitoGetWatchList)
            }
        }
        catch{
        }
    }
    
    func callApitoGetWatchList(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    //let msg = json["msg"] as! String
                    if(status == "true"){
                        let video = json["video"] as! [NSDictionary]
                        self.watchVideosList = video
                        
                        self.collWatchList.register(UINib(nibName: "WatchListCell", bundle: nil), forCellWithReuseIdentifier: "WatchListCell")
                        self.collWatchList.delegate = self
                        self.collWatchList.dataSource = self
                        
                        if self.watchVideosList.count == 0{
                            //self.viewCollection.isHidden = true
                            
                        }else{
                            //self.viewCollection.isHidden = false
                            self.collWatchList.reloadData()
                        }

                    }
                    else{
                        self.watchVideosList = []
                        self.collWatchList.reloadData()
                       // ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}

extension watchListViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.watchVideosList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WatchListCell", for: indexPath) as! WatchListCell
        
        let dict = self.watchVideosList[indexPath.row]
        
        let title = dict["title"] as! String
        cell.lblEpisodeName.text = title
        
        let image = dict["image"] as? String
        
        cell.imgVideo.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        let url = image!.replacingOccurrences(of: " ", with: "%20")
        cell.imgVideo.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "rsz_2picsart_04-30-85410"))

        cell.btnCross.tag = indexPath.row
        cell.btnCross.addTarget(self, action: #selector(clickOnCrossBtn(_:)), for: .touchUpInside)
        
        return cell
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.watchVideosList[indexPath.row]
        let video = dict["video"] as! String
        let vId = dict["videoid"] as! String
        self.playVideos(url: video, videoId: vId,data: dict)
    }
    
    func playVideos(url : String, videoId: String, data: NSDictionary){
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController")as? VideoPlayerViewController
        controller?.videoUrl = url as NSString
        controller?.videoDict = data
        controller?.videoPlayFrom = "watch"
        self.navigationController?.pushViewController(controller!, animated: true)
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2, height: 250)
    }
    
    @objc func clickOnCrossBtn(_ sender: UIButton)
    {
        showIndicator(withTitle: "", and: "")
        let dict = self.watchVideosList[sender.tag]
            do {
                let decoded  = userDef.object(forKey: "userInfo") as! Data
                if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let uId = responseDict["id"] as! String
                    let vId = dict["videoid"] as! String
                    let params = [ "videoid": vId,
                                   "id": uId] as [String : Any]
                    
                    let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.playlistremove)"
                    let url = URL(string: strUrl)
                    ApiService.callPost(url: url!, params: params, finish: callApitoRemoveVideo)
                }
            }
            catch{
                
            }
    }

    func callApitoRemoveVideo(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    let msg = json["msg"] as! String
                    if(status == "true"){
                        self.viewWillAppear(true)
                        //ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                        
                    }
                    else{
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }


    
}


