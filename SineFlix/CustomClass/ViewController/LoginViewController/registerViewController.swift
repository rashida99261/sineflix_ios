//
//  registerViewController.swift
//  SineFlix
//
//  Created by Apple on 04/02/21.
//

import UIKit
import Alamofire


class registerViewController: UIViewController {
    
    
    
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPaswrd : UITextField!
    @IBOutlet weak var profileImg : UIImageView!
    
    
    @IBOutlet weak var btnMale : UIButton!
    @IBOutlet weak var btnFemale : UIButton!
    @IBOutlet weak var btnAge : UIButton!
    @IBOutlet weak var btnTerms : UIButton!
    
    var gender = 1
    var age = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnTerms.isSelected = true
        self.btnMale.isSelected = true
        self.btnFemale.isSelected = false
        self.btnAge.isSelected = false
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.isHidden = false
    }
    
    
    
    
    @IBAction func backToViewController(sendr : Any){
        self.navigationController?.popViewController(animated: true)
    }
    
}






extension registerViewController  {
    
    
    @IBAction func SubmitBtn(sender : Any){
        
        if(txtName.text == "" || txtName.text?.count == 0 || txtName.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill name field.")
        }
        else if(txtMobile.text == "" || txtMobile.text?.count == 0 || txtMobile.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill mobile field.")
        }
        else if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill email field.")
        }
        else if(!(ApiService.isValidEmail(testStr: txtEmail.text!))){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill valid email field.")
        }
        else if(txtPaswrd.text == "" || txtPaswrd.text?.count == 0 || txtPaswrd.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill password field.")
        }
        else if(self.age == false){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please confirm your age.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            showIndicator(withTitle: "", and: "")
            self.callLoginApi()
        }
        else{
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "PLease check your internet connection.")
        }
    }
    
    func callLoginApi(){
        
        // , , , , usertype, profile_image
        
        let params = [ "name": self.txtName.text!,
                       "email": self.txtEmail.text!,
                       "gender": self.gender,
                       "mobile": self.txtMobile.text!,
                       "password": self.txtPaswrd.text!] as [String : Any]
        
        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.usercreate)"
        let url = URL(string: strUrl)
        ApiService.callPost(url: url!, params: params, finish: finishPost)
        
    }
    
    func finishPost (message:String, data:Data?) -> Void
    {
        OperationQueue.main.addOperation {
            
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "User registered successfully!")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
            
            
        }
    }
    
}

extension registerViewController  {
    
    @IBAction func clickOnGenderBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if btnMale.isSelected == true {
                btnMale.isSelected = false
                btnFemale.isSelected = true
                self.gender = 2
                
            }else {
                btnFemale.isSelected = false
                btnMale.isSelected = true
                self.gender = 1
            }
            
        }
        else if(sender.tag == 20){
            if btnFemale.isSelected == true {
                btnFemale.isSelected = false
                btnMale.isSelected = true
                self.gender = 1
                
            }else {
                btnMale.isSelected = false
                btnFemale.isSelected = true
                self.gender = 2
            }
        }
    }
    
    @IBAction func clickOnAgeBtn(_ sender: UIButton)
    {
        if btnAge.isSelected == true {
            btnAge.isSelected = false
            self.age = false
            
        }else {
            btnAge.isSelected = true
            self.age = true
        }
        
        
    }
}
