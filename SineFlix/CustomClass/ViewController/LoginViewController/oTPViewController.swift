//
//  oTPViewController.swift
//  SineFlix
//
//  Created by Apple on 08/02/21.
//

import UIKit

class oTPViewController: UIViewController {
    
    
    @IBOutlet weak var txtOtp : UITextField!
    @IBOutlet weak var password : UITextField!
    @IBOutlet weak var Confrm_password : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.isHidden = false
    }
    
    
    
    
    @IBAction func backToViewController(sendr : Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnsubmit(sender : UIButton){
        
        
        if(txtOtp.text == "" || txtOtp.text?.count == 0 || txtOtp.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill OTP field.")
        }
        else if(password.text == "" || password.text?.count == 0 || password.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill password field.")
        }
        else if(Confrm_password.text == "" || Confrm_password.text?.count == 0 || Confrm_password.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill password field again.")
        }
        else if(password.text != Confrm_password.text){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Password and Confirm Password do not match.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            showIndicator(withTitle: "", and: "")
            self.callChnagePassApi()
        }
        else{
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "PLease check your internet connection.")
        }
        
        
    }
    
    func callChnagePassApi(){
        let params = [ "otp": self.txtOtp.text!,
                       "password": self.password.text!] as [String : Any]
        
        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.otpmatch)"
        let url = URL(string: strUrl)
        ApiService.callPost(url: url!, params: params, finish: finishPost)
        
    }
    
    func finishPost (message:String, data:Data?) -> Void
    {
        OperationQueue.main.addOperation {
            
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    let msg = json["msg"] as! String
                    if(status == "true"){
                        
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                        
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: LoginViewController.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                    else{
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                    
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
}
