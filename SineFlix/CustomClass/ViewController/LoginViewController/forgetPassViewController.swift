//
//  forgetPassViewController.swift
//  SineFlix
//
//  Created by Apple on 04/02/21.
//

import UIKit

class forgetPassViewController: UIViewController {
    
    @IBOutlet weak var loginEmail : UITextField!
    @IBOutlet weak var logoImg : UIImageView!
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.isHidden = false
    }
    
    
    @IBAction func Submit(sender : Any){
        
        if(loginEmail.text == "" || loginEmail.text?.count == 0 || loginEmail.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill email field.")
        }
        else if(!(ApiService.isValidEmail(testStr: loginEmail.text!))){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill valid email field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            showIndicator(withTitle: "", and: "")
            self.callForgetPassApi()
        }
        else{
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "PLease check your internet connection.")
        }

    }
    
    func callForgetPassApi(){
        let params = [ "email": self.loginEmail.text!] as [String : Any]
        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.mailverify)"
        let url = URL(string: strUrl)
        ApiService.callPost(url: url!, params: params, finish: finishPost)
    }
    
    func finishPost (message:String, data:Data?) -> Void
    {
        OperationQueue.main.addOperation {
            
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    let status = json["status"] as! String
                    let msg = json["msg"] as! String
                    if(status == "true"){
                        
                        let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title: "OK", style: .default) {
                            UIAlertAction in
                            let controller = self.storyboard?.instantiateViewController(identifier: "oTPViewController")as? oTPViewController
                            self.navigationController?.pushViewController(controller!, animated: true)
                        }
                        // Add the actions
                        alertController.addAction(okAction)
                        // Present the controller
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                    
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }

            
        }
    }
    
}


