//
//  watchVideoViewController.swift
//  SineFlix
//
//  Created by Apple on 07/02/21.
//

import UIKit
import SDWebImage
import Photos

class watchVideoViewController: UIViewController {

    @IBOutlet weak var playerView : AGVideoPlayerView!
    
    @IBOutlet weak var viewSuggesion : UIView!
    @IBOutlet weak var viewSuggHgt : NSLayoutConstraint!
    @IBOutlet weak var collSuggesion : UICollectionView!
    
    var videoUrl : NSString = ""
    var videoData : video!
    var videoDict: NSDictionary = [:]
    var videoTitle = ""
    
    var videoPlayFrom = ""
    
    var arrSuggestion : [NSDictionary] = []
    
    var arrAllEpisode : [NSDictionary] = []
    
    @IBOutlet weak var publishDate : UILabel!
    @IBOutlet weak var songSubtitle : UILabel!
    @IBOutlet weak var songDetails : UILabel!

    var isClickOnEpisode = false
    var newVideoId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var urlStr = videoUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        print(urlStr)
        playerView.videoUrl = URL.init(string: urlStr!)
        
        playerView.shouldAutoplay = true
        playerView.shouldAutoRepeat = true
        playerView.showsCustomControls = false
        playerView.shouldSwitchToFullscreen = true
        
        self.viewSuggesion.isHidden = true
        self.viewSuggHgt.constant = 0
        
        self.setUpDataomUI()
        
    }
    
    func setUpDataomUI(){
        
        var videoId = ""
        
        
        
        if(videoPlayFrom == "watch"){
            self.publishDate.text = "Published on " + "\(videoDict["cdate"] as! String)"
            self.songSubtitle.text = ""
            self.songDetails.text = (videoDict["description"] as! String)
            videoId = self.videoDict["videoid"] as! String
            videoTitle = self.videoDict["title"] as! String
        }
        else if(videoPlayFrom == "other"){
            self.publishDate.text = "Published on " + videoData.cdate!
            self.songSubtitle.text = ""
            self.songDetails.text = videoData.description
            videoId = self.videoData.videoid!
            videoTitle = self.videoData.title!
        }
        
        self.getAllEpisodesForVideo(strVideId: videoId)
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }

    }
    
}
extension watchVideoViewController {
    
    @IBAction func clickONBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            
            if(isClickOnEpisode == false){
                if(videoPlayFrom == "watch"){
                    self.newVideoId = self.videoDict["videoid"] as! String
                }
                else if(videoPlayFrom == "other"){
                    self.newVideoId = self.videoData.videoid!
                }
            }
            
            showIndicator(withTitle: "", and: "")
                
                do {
                    let decoded  = userDef.object(forKey: "userInfo") as! Data
                    if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                        let uId = responseDict["id"] as! String
                        let params = [ "videoid": self.newVideoId,
                                       "id": uId] as [String : Any]
                        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.playlist)"
                        let url = URL(string: strUrl)
                        ApiService.callPost(url: url!, params: params, finish: callApitoSaveVideoInWatchList)
                    }
                }
                catch{
                    
                }
        }
        else if(sender.tag == 20){
            let urlStr = videoUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
            let Itunes_url = URL(string: urlStr!)!
            let shareAll = [Itunes_url] as [Any]
            let ac = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            self.present(ac, animated: true, completion: nil)
        }
        else if(sender.tag == 30){
            
//            showIndicator(withTitle: "", and: "")
//            let sampleURL = self.videoUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
//            DispatchQueue.global(qos: .background).async {
//                if let url = URL.init(string: sampleURL!), let urlData = NSData(contentsOf: url) {
//                    let galleryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
//                    let filePath="\(galleryPath)/\(self.videoTitle).mp4"
//                    DispatchQueue.main.async {
//                       urlData.write(toFile: filePath, atomically: true)
//                          PHPhotoLibrary.shared().performChanges({
//                          PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL:
//                          URL(fileURLWithPath: filePath))
//                       }) {
//                          success, error in
//                          if success {
//
//                            OperationQueue.main.addOperation {
//                                self.hideIndicator()
//                                    //create json object from data
//                                ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Video downloaded successfully.")
//                            }
//                          } else {
//                            print(error?.localizedDescription)
//                          }
//                       }
//                    }
//                 }
//              }
        }
    }
    
    func callApitoSaveVideoInWatchList(message:String, data:Data?) -> Void {

        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    let msg = json["msg"] as! String
                    if(status == "true"){
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                    else{
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}


extension watchVideoViewController  {
    
    func getAllEpisodesForVideo(strVideId: String){
        showIndicator(withTitle: "", and: "")
        let params = [ "video_id": strVideId] as [String : Any]
        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.videodetails)"
        let url = URL(string: strUrl)
        ApiService.callPost(url: url!, params: params, finish: parseApiResponse)
    }
    
    func parseApiResponse(message:String, data:Data?) -> Void {

        OperationQueue.main.addOperation {
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    //let msg = json["msg"] as! String
                    if(status == "true"){
                        let suggestion = json["suggestion"] as! [NSDictionary]
                        self.arrSuggestion = suggestion
                        
                        self.collSuggesion.register(UINib(nibName: "episodesCell", bundle: nil), forCellWithReuseIdentifier: "episodesCell")
                        self.collSuggesion.delegate = self
                        self.collSuggesion.dataSource = self
                        
                        if self.arrSuggestion.count == 0{
                            self.viewSuggesion.isHidden = true
                            self.viewSuggHgt.constant = 0
                            
                        }else{
                            self.viewSuggesion.isHidden = false
                            self.viewSuggHgt.constant = 375
                            self.collSuggesion.reloadData()
                        }

                    }
                    else{
                        self.viewSuggesion.isHidden = true
                        self.viewSuggHgt.constant = 0
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}

extension watchVideoViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrSuggestion.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "episodesCell", for: indexPath) as? episodesCell else { return UICollectionViewCell() }
        
            let obj = self.arrSuggestion[indexPath.row]
            let image = obj["url_thumbnail"] as? String
            cell.imgNews.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            let url = image!.replacingOccurrences(of: " ", with: "%20")
            cell.imgNews.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "rsz_2picsart_04-30-85410"))
        
            cell.imgNews.clipsToBounds = true
            cell.imgNews.layer.cornerRadius = 12
            cell.imgNews.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]

            let title = obj["title"] as? String
            cell.lblTitle.text = title

            let description = obj["description"] as? String
            cell.lblDescription.text = description
            
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let obj = self.arrSuggestion[indexPath.row]
        
        print(obj)
        
        self.newVideoId = obj["videoid"] as! String
        
        let description = obj["description"] as? String
        self.songDetails.text = description
        
        self.videoTitle = obj["title"] as! String
        
        self.videoUrl = obj["url"] as! NSString
        var urlStr = self.videoUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        playerView.videoUrl = URL.init(string: urlStr!)
        
        playerView.shouldAutoplay = true
        playerView.shouldAutoRepeat = true
        playerView.showsCustomControls = false
        playerView.shouldSwitchToFullscreen = true

        isClickOnEpisode = true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collSuggesion.frame.width - 40, height: 300)
    }
}
