//
//  NewReleaseViewCell.swift
//  SineFlix
//
//  Created by Gupta Ji on 31/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit

class NewReleaseViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail : UIImageView!
    @IBOutlet weak var title : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnail.layer.cornerRadius = thumbnail.frame.size.width/2.0
        thumbnail.layer.masksToBounds = true
    }

}
