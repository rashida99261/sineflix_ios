//
//  ViewController.swift
//  Testneev
//
//  Created by Gupta Ji on 27/12/2019.
//  Copyright © 2019 Vivek Gupta. All rights reserved.
//

import UIKit
import StoreKit
//import SideMenuSwift
//import Alamofire


//class Preferences {
//    static let shared = Preferences()
//    var enableTransitionAnimation = false
//}

class MenuViewController: UIViewController {
    var isDarkModeEnabled = false
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
            //tableView.layer.cornerRadius = 18
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.reloadData()
        
    }
    
    
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = userDef.value(forKey: "session") as? String {
            return Endpoints.Environment.arrMenuLogin.count
        }
        else{
            return Endpoints.Environment.arrMenuLogout.count
        }
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SelectionCell
        
        var dict = NSDictionary()
        
        if let _ = userDef.value(forKey: "session") as? String {
            dict = Endpoints.Environment.arrMenuLogin[indexPath.row] as NSDictionary
        }
        else{
            dict = Endpoints.Environment.arrMenuLogout[indexPath.row] as NSDictionary
        }
        
        cell.titleLabel?.text = dict["title"] as? String
        let strImg = dict["img"] as? String
        cell.imgMenu.image = UIImage(named: strImg!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = userDef.value(forKey: "session") as? String {
            self.clickToMove(indexPath)
        }
        else{
            self.clickToMoveBeforeLogin(indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func menuAction(sender : Any){
        //        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")
        //        self.navigationController?.pushViewController(controller!, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    func clickToMove(_ indexPath: IndexPath){
        if(indexPath.row == 0){
        }
        else if(indexPath.row == 1){
            let controller = self.storyboard?.instantiateViewController(identifier: "legalViewController")as? legalViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 2){
            let controller = self.storyboard?.instantiateViewController(identifier: "subscribeViewController")as? subscribeViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 3){
            let controller = self.storyboard?.instantiateViewController(identifier: "AboutViewController")as? AboutViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 4){
            let controller = self.storyboard?.instantiateViewController(identifier: "watchListViewController")as? watchListViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 5){
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview(in: UIApplication.shared.currentScene!)
            } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "appId") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else if(indexPath.row == 6){
            let iTunesLink = "https://apps.apple.com/app/EBONi/id1544792703" //need to change url
            let Itunes_url = URL(string: iTunesLink)!
            let strShare_1 = "Download Here: \(Itunes_url)"
            let shareAll = [strShare_1] as [Any]
            let ac = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            self.present(ac, animated: true, completion: nil)
        }
        else if(indexPath.row == 7){
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "contactUsViewController") as? contactUsViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 8){
            
            let alertController = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: .default) {
                UIAlertAction in
                userDef.removeObject(forKey: "userInfo")
                userDef.removeObject(forKey: "session")
                let controller = self.storyboard?.instantiateViewController(identifier: "ViewController")as? ViewController
                self.navigationController?.pushViewController(controller!, animated: false)
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
                self.dismiss(animated: true, completion: nil)
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func clickToMoveBeforeLogin(_ indexPath: IndexPath)
    {
        if(indexPath.row == 0){
            let controller = self.storyboard?.instantiateViewController(identifier: "legalViewController")as? legalViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 1){
            
//            if let _ = userDef.value(forKey: "session") as? String {
//                
//            }
//            else{
                let alertController = UIAlertController(title: "", message: "Please Login First", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            //}
        }
        else if(indexPath.row == 2){
            let controller = self.storyboard?.instantiateViewController(identifier: "AboutViewController")as? AboutViewController
            self.navigationController?.pushViewController(controller!, animated: true)
            
        }
        else if(indexPath.row == 3){
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview(in: UIApplication.shared.currentScene!)
            } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "appId") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else if(indexPath.row == 4){
            
            let iTunesLink = "https://apps.apple.com/app/EBONi/id1544792703" //need to change url
            let Itunes_url = URL(string: iTunesLink)!
            
            let strShare_1 = "Download Here: \(Itunes_url)"
            let shareAll = [strShare_1] as [Any]
            
            let ac = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            self.present(ac, animated: true, completion: nil)
        }
        else if(indexPath.row == 5){
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "contactUsViewController") as? contactUsViewController
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(indexPath.row == 6){
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            controller.isLoginFrom = "menu"
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}

class SelectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgMenu: UIImageView!
}

extension UIApplication {
    var currentScene: UIWindowScene? {
        connectedScenes
            .first { $0.activationState == .foregroundActive } as? UIWindowScene
    }
}
