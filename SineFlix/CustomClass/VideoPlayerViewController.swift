//
//  VideoPlayerViewController.swift
//  SineFlix
//
//  Created by Gupta Ji on 31/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit

class VideoPlayerViewController: UIViewController {
    
    @IBOutlet weak var playerView : AGVideoPlayerView!
    @IBOutlet weak var tblView : UITableView!
    var videoUrl : NSString = ""
    var videoData : video!
    var videoDict: NSDictionary = [:]
    
    var videoPlayFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblView.rowHeight = UITableView.automaticDimension;
        self.tblView.estimatedRowHeight = 44.0;
        
        var urlStr = videoUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        print(urlStr)
        playerView.videoUrl = URL.init(string: urlStr!)
        
        print(playerView.videoUrl)
        
        playerView.shouldAutoplay = true
        playerView.shouldAutoRepeat = true
        playerView.showsCustomControls = false
        playerView.shouldSwitchToFullscreen = true

    }
    
    
}
extension VideoPlayerViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? VideoDetailsCell
        
        if(videoPlayFrom == "watch"){
            cell?.videoDict = videoDict
            cell?.publishDate.text = "Published on " + "\(videoDict["cdate"] as! String)"
            cell?.songSubtitle.text = ""
            cell?.songDetails.text = (videoDict["description"] as! String)
        }
        else if(videoPlayFrom == "other"){
            cell!.videoData = videoData
            cell!.publishDate.text = "Published on " + videoData.cdate!
            cell!.songSubtitle.text = ""
            cell!.songDetails.text = videoData.description
        }
        
        cell!.btnWatchList.tag = indexPath.row
        cell!.btnWatchList.addTarget(self, action: #selector(self.clickOnWatchList(_:)), for: .touchUpInside)
        
        cell!.btnShare.tag = indexPath.row
        cell!.btnShare.addTarget(self, action: #selector(self.clickOnShare(_:)), for: .touchUpInside)
        
        cell!.btnWatchVideo.tag = indexPath.row
        cell!.btnWatchVideo.addTarget(self, action: #selector(self.clickOnWatchVideo(_:)), for: .touchUpInside)
        
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func moveToBackViewController(sender :UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func clickOnShare(_ sender: UIButton){
        
        let urlStr = videoUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        let Itunes_url = URL(string: urlStr!)!
        let shareAll = [Itunes_url] as [Any]
        let ac = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        self.present(ac, animated: true, completion: nil)
        
    }
    
    @objc func clickOnWatchList(_ sender: UIButton){
        
        var videoId = ""
        if(videoPlayFrom == "watch"){
            videoId = self.videoDict["videoid"] as! String
        }
        else if(videoPlayFrom == "other"){
            videoId = self.videoData.videoid!
        }
        
        if let _ = userDef.value(forKey: "session") as? String {
            
            do {
                let decoded  = userDef.object(forKey: "userInfo") as! Data
                if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let uId = responseDict["id"] as! String
                    let params = [ "videoid": videoId,
                                   "id": uId] as [String : Any]
                    let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.playlist)"
                    let url = URL(string: strUrl)
                    ApiService.callPost(url: url!, params: params, finish: callApitoSaveVideoInWatchList)
                }
            }
            catch{
                
            }
        }
        else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            controller.isLoginFrom = "video"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    
    func callApitoSaveVideoInWatchList(message:String, data:Data?) -> Void {
        
        OperationQueue.main.addOperation {
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    let msg = json["msg"] as! String
                    if(status == "true"){
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                    else{
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func clickOnWatchVideo(_ sender: UIButton){
        
        if let _ = userDef.value(forKey: "session") as? String {
            
            //check subscribe user then navigate
            
            let controller = self.storyboard?.instantiateViewController(identifier: "watchVideoViewController") as? watchVideoViewController
            if(videoPlayFrom == "watch"){
                controller?.videoDict = videoDict
            }
            else if(videoPlayFrom == "other"){
                controller?.videoData = videoData
            }
            controller?.videoUrl = videoUrl
            controller?.videoPlayFrom = videoPlayFrom
            self.navigationController?.pushViewController(controller!, animated: false)
            
        }
        else{
            let alertController = UIAlertController(title: "", message: "Please Login First", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) {
                UIAlertAction in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}

class VideoDetailsCell: UITableViewCell, UITextViewDelegate{
    
    @IBOutlet weak var btnWatchList : UIButton!
    @IBOutlet weak var btnShare : UIButton!
    @IBOutlet weak var btnWatchVideo : UIButton!
    
    @IBOutlet weak var publishDate : UILabel!
    //@IBOutlet weak var songtitle : UILabel!
    @IBOutlet weak var songSubtitle : UILabel!
    @IBOutlet weak var songDetails : UILabel!
    var videoData : video!
    var videoDict : NSDictionary = [:]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}

