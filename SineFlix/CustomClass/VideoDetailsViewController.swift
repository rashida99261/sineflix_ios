//
//  VideoDetailsViewController.swift
//  SineFlix
//
//  Created by Gupta Ji on 10/06/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit
import SDWebImage

class VideoDetailsViewController: UIViewController,  UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView : UICollectionView!
    var seeMoreVideosList : [video] = [video]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "VideosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VideosCollectionViewCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seeMoreVideosList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideosCollectionViewCell", for: indexPath as IndexPath) as? VideosCollectionViewCell
        cell?.thumbnail.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        
        let url = seeMoreVideosList[indexPath.row].image!.replacingOccurrences(of: " ", with: "%20")
        cell?.thumbnail.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "rsz_2picsart_04-30-85410"))
        cell?.title.text = seeMoreVideosList[indexPath.row].title
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 20)/2, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController")as? VideoPlayerViewController
        controller?.videoUrl = seeMoreVideosList[indexPath.row].video! as NSString
        controller?.videoData = seeMoreVideosList[indexPath.row]
        controller?.videoPlayFrom = "other"
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @IBAction func moveToBackViewController(sender :UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
