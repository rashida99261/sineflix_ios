//
//  VideosTableViewCellTableViewCell.swift
//  SinFlix
//
//  Created by Gupta Ji on 30/05/2020.
//  Copyright © 2020 Vivek Gupta. All rights reserved.
//

import UIKit
import SDWebImage

protocol videoPlayerDelegate: class {
    func playVideos(url : String, videoId: String, data: video)
}

class NewRVideosTableViewCell: UITableViewCell, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var rightTitle : UIButton!
    @IBOutlet weak var leftTitle : UILabel!
    var sliderVideosList : [video] = [video]()
    
    var delegate : videoPlayerDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.register(UINib.init(nibName: "NewReleaseViewCell", bundle: nil), forCellWithReuseIdentifier: "NewReleaseViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //leftTitle.text = "jhjjh"
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderVideosList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewReleaseViewCell", for: indexPath as IndexPath) as? NewReleaseViewCell
        cell?.title.text = ""
        
        let url =  sliderVideosList[indexPath.row].image!.replacingOccurrences(of: " ", with: "%20")
        cell?.thumbnail.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell?.thumbnail.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "rsz_2picsart_04-30-85410"))
        cell?.title.text = sliderVideosList[indexPath.row].title
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: 120, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.playVideos(url: sliderVideosList[indexPath.row].video!, videoId:  sliderVideosList[indexPath.row].videoid!,data: sliderVideosList[indexPath.row])
    }
}
