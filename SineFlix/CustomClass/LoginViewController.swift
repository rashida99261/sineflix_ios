
import UIKit
import Alamofire
import AuthenticationServices
import KeychainSwift

class LoginViewController: UIViewController {
    
    var userType : String?
    
    @IBOutlet weak var loginId : UITextField!
    @IBOutlet weak var password : UITextField!
    @IBOutlet weak var logoImg : UIImageView!
    
    var isLoginFrom = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //loginId.text = "lalitkumar9526@gmail.com"
        //password.text = "1234567"
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.isHidden = false
    }
    
    
    @IBAction func backToViewController(sendr : Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func login(sender : Any){
        
        if(loginId.text == "" || loginId.text?.count == 0 || loginId.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill email field.")
        }
        else if(!(ApiService.isValidEmail(testStr: loginId.text!))){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill valid email field.")
        }
        else if(password.text == "" || password.text?.count == 0 || password.text == nil){
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "Please fill password field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            showIndicator(withTitle: "", and: "")
            self.callLoginApi()
        }
        else{
            ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: "PLease check your internet connection.")
        }
    }
    
    func callLoginApi(){
        
        let params = [ "mobile": self.loginId.text!,
                       "password": self.password.text!] as [String : Any]
        
        let strUrl = "\(Endpoints.Environment.baseUrl)/\(URLPath.login)"
        let url = URL(string: strUrl)
        ApiService.callPost(url: url!, params: params, finish: finishPost)

    }
    
    func finishPost (message:String, data:Data?) -> Void
    {
        OperationQueue.main.addOperation {
            
            self.hideIndicator()
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    let status = json["status"] as! String
                    let msg = json["msg"] as! String
                    if(status == "true"){
                        if let user = json["user"] as? NSDictionary{
                            print(user)
                            do {
                                if #available(iOS 11.0, *) {
                                    let myData = try NSKeyedArchiver.archivedData(withRootObject: user, requiringSecureCoding: false)
                                    userDef.set(myData, forKey: "userInfo")
                                } else {
                                    let myData = NSKeyedArchiver.archivedData(withRootObject: user)
                                    userDef.set(myData, forKey: "userInfo")
                                }
                            } catch {
                                print("Couldn't write file")
                            }
                            
                            userDef.set("true", forKey: "session")
                            
                            if(self.isLoginFrom == "menu"){
                                let controller = self.storyboard?.instantiateViewController(identifier: "ViewController")as? ViewController
                                self.navigationController?.pushViewController(controller!, animated: false)
                            }
                            else if(self.isLoginFrom == "video"){
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                    else{
                        ApiService.showAlertMessage(vc: self, titleStr: "", messageStr: msg)
                    }
                    
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }

            
        }
    }
    
}

extension LoginViewController  {
    
    @IBAction func clickOnBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            
            let controller = self.storyboard?.instantiateViewController(identifier: "registerViewController")as? registerViewController
            self.navigationController?.pushViewController(controller!, animated: false)

        }
        else if(sender.tag == 20){
            let controller = self.storyboard?.instantiateViewController(identifier: "forgetPassViewController")as? forgetPassViewController
            self.navigationController?.pushViewController(controller!, animated: true)

        }
    }
    
    
}
