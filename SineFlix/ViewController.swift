//
//  ViewController.swift
//  PageMenuDemoStoryboard
//
//  Created by Niklas Fahl on 12/19/14.
//  Copyright (c) 2014 CAPS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.orange]
//
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "<-", style: UIBarButtonItem.Style.done, target: self, action: #selector(ViewController.didTapGoToLeft))
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "->", style: UIBarButtonItem.Style.done, target: self, action: #selector(ViewController.didTapGoToRight))
//
        // MARK: - Scroll menu setup
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
//        let controller1 : TestTableViewController = TestTableViewController(nibName: "TestTableViewController", bundle: nil)
//        controller1.title = "FRIENDS"
//        controllerArray.append(controller1)
//        let controller2 : TestCollectionViewController = TestCollectionViewController(nibName: "TestCollectionViewController", bundle: nil)
//        controller2.title = "MOOD"
//        controllerArray.append(controller2)
//        let controller3 : TestViewController = TestViewController(nibName: "TestViewController", bundle: nil)
//        controller3.title = "MUSIC"
//        controllerArray.append(controller3)
//        let controller4 : TestViewController = TestViewController(nibName: "TestViewController", bundle: nil)
//        controller4.title = "FAVORITES"
//        controllerArray.append(controller4)
        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
        homeViewController!.title = "HOME"
        controllerArray.append(homeViewController!)
        
        let moviesViewController = self.storyboard?.instantiateViewController(withIdentifier: "MoviesViewController")
        moviesViewController!.title = "MOVIES"
        controllerArray.append(moviesViewController!)
        
        let webShowViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebShowViewController")
        webShowViewController!.title = "WEB SHOW"
        controllerArray.append(webShowViewController!)
        
        let videosViewController = self.storyboard?.instantiateViewController(withIdentifier: "VideosViewController")
        videosViewController!.title = "VIDEOS"
        controllerArray.append(videosViewController!)
        
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .bottomMenuHairlineColor(UIColor.white),
            .menuItemSeparatorColor(UIColor.clear),
            .scrollMenuBackgroundColor(UIColor.red),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.white),
            //.menuHeight(50),
            //.centerMenuItems(true),
            .menuItemFont(UIFont.boldSystemFont(ofSize: 14)),
            .menuItemSeparatorWidth(4.3),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        //pageMenu!.delegate = self
        pageMenu!.didMove(toParent: self)
        navigationController?.navigationBar.barTintColor = UIColor.red
        // Customize menu (Optional)
//        let parameters: [CAPSPageMenuOption] = [
//            .scrollMenuBackgroundColor(UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)),
//            .viewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
//            .selectionIndicatorColor(UIColor.orange),
//            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
//            .menuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
//            .menuHeight(40.0),
//            .menuItemWidth(90.0),
//            .centerMenuItems(true)
//        ]
//
//        // Initialize scroll menu
//        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
//
//        self.addChild(pageMenu!)
//        self.view.addSubview(pageMenu!.view)
//
//        pageMenu!.didMove(toParent: self)
    }
    
//    @objc func didTapGoToLeft() {
//        let currentIndex = pageMenu!.currentPageIndex
//
//        if currentIndex > 0 {
//            pageMenu!.moveToPage(currentIndex - 1)
//        }
//    }
//
//    @objc func didTapGoToRight() {
//        let currentIndex = pageMenu!.currentPageIndex
//
////        if currentIndex < pageMenu!.controllerArray.count {
////            pageMenu!.moveToPage(currentIndex + 1)
////        }
//    }
    
    // MARK: - Container View Controller

    //COULD NOT RESOLVE
//    override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
//        return true
//    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
    @IBAction func menuAction(sender : Any){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController")
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @IBAction func searchAction(sender : Any){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "searchViewController")
        self.navigationController?.pushViewController(controller!, animated: true)
    }

}


